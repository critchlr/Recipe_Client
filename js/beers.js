/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute'])

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
		.when('/register', {
		  templateUrl: 'templates/register.html',
      controller: 'registerController'
		})
    .when('/detail/:name', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
		.when('/login', {
		  templateUrl: 'templates/login.html',
      controller: 'loginController'
		})
		.otherwise({
		  redirectTo: 'login'
		})
	}])

.run(function($rootScope, $location) {
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
      if (localStorage.user === undefined) {
        $location.path("/login")
      }
    });
  });

myApp.controller('searchController', function($scope, $http) {
  $scope.message = 'Search for a beer'
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
      console.log(search)
      var url = 'https://recipeapi-critchlruni.c9.io/beers/'+search
      $http.get(url).success(function(response) {
        console.log(response)
        console.log(response.data.data[0])
        $scope.beers = response.data.data[0]
        console.log(response.data.data[0].name)
        
      })
    }
  }
})

myApp.controller('loginController', function($scope, $http, $route, $window, $location) {
$scope.login = function($event) {
  var username = $scope.username
  var password = $scope.password
var url = 'https://recipeapi-critchlruni.c9.io/login/'+username + '/' + password
$http.get(url).success(function(data){
  if (data.data[0] !== undefined) {
        console.log('username and pass exists')
        localStorage.setItem("user", data.data[0].user)
        localStorage.setItem("password", data.data[0].password)
        $route.reload()
        $location.path('/search')
      } else {
        $scope.logErrorMessage = 'Incorrect log in credentials!'
        console.log('no username and pass exists');
      }
    })
    
  }
  $scope.logout = function($event) {
  localStorage.clear()
  $route.reload()
    
  }
  $scope.registerAccount = function() {
    console.log("HERE!!!!!!!");
    
    var username = $scope.newusername;
      console.log(username);
    var password =  $scope.newpassword;
      console.log(password);

      
    var url ='https://recipeapi-critchlruni.c9.io/login/post/'+ username + '/' + password
    $http.post(url).success(function(data, status, headers, config) {
        console.log(data)
        console.log(status)
        $route.reload()
  console.log('success')
  }) .error(function(data, status, headers, config) {
    console.log('error')
  })
  }
})


myApp.controller('detailController', function($scope, $routeParams, $http) {
  $scope.message = 'This is the detail screen'
  $scope.name = $routeParams.name

   $scope.postRatingBeer = function() {
    console.log("HERE!!!!!!!");
    
    var name =  $scope.name;
      console.log(name);
    var rating =  $scope.rating;
      console.log(rating);
      var comments =  $scope.comments;
      console.log(comments);
      
    var url ='https://recipeapi-critchlruni.c9.io/beers/post/'+ name + '/' + rating + '/' + comments
    $http.post(url).success(function(data, status, headers, config) {
        console.log(data)
        console.log(status)
  console.log('success')
  }) .error(function(data, status, headers, config) {
    console.log('error')
  })
  }
})

 
myApp.controller('favouritesController', function($scope, $http, $route) {
  $http({
        method: 'GET',
        url:'https://recipeapi-critchlruni.c9.io/bartab'
      }).success(function(data, status, headers, config) {
        console.log(data.data);
            $scope.beers = data.data;
         
  }) .error(function(data, status, headers, config) {
    console.log('error');
  });
  
  
  
  $scope.delete = function(beer) {
    console.log("HERE!!!!!!!");
    var id =  beer.id;
      console.log(id);
      
    $http({
        method: 'DELETE',
        url:'https://recipeapi-critchlruni.c9.io/bartab/delete/'+ id  
      }).success(function(data, status, headers, config) {
        console.log(data);
        console.log(status);
        alert("The following game record has been removed: " + beer.name );
        $route.reload();
  }) .error(function(data, status, headers, config) {
    console.log('error');
  })
  }
  $scope.update = function(beer, newrating) {
    console.log("HERE!!!!!!!");
    var id =  beer.id;
      console.log(id);
      console.log(newrating); 
      $http({
        method: 'PUT',
        url:'https://recipeapi-critchlruni.c9.io/bartab/'+ id + '/' + newrating
      }).success(function(data, status, headers, config) {
        console.log(data);
        console.log(status);
        alert("The following game record has been updated: " + beer.name );
        $route.reload();
  }) .error(function(data, status, headers, config) {
    console.log('error');
  });
}

$scope.update1 = function(beer, newcomments) {
    console.log("HERE!!!!!!!");
    var id =  beer.id
      console.log(id)
      console.log(newcomments);
   
$http({
        method: 'PUT',
        url:'https://recipeapi-critchlruni.c9.io/bartab/'+ id + '/'+  newcomments
      }).success(function(data, status, headers, config) {
        console.log(data)
        console.log(status)
        alert("The following game record has been updated: " + beer.name );
        $route.reload()
  }) .error(function(data, status, headers, config) {
    console.log('error')
   })
}
})
